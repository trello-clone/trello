<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
  'prefix'    => 'auth',
  'namespace' => 'Api'
], function () {
  Route::post('login', 'AuthController@login');
  Route::post('register', 'AuthController@register');
  Route::get('register/activate/{token}', 'AuthController@registerActivate');

  Route::group(['middleware' => 'auth:api'], function () {
    Route::get('logout', 'AuthController@logout');
    Route::get('me', 'AuthController@me');
    Route::put('profile', 'UserController@updateProfile');
    Route::put('password', 'UserController@updatePassword');
  });
});

Route::group([
  'namespace'  => 'Api',
  'middleware' => 'api',
  'prefix'     => 'password'
], function () {
  Route::post('create', 'PasswordResetController@create');
  Route::get('find/{token}', 'PasswordResetController@find');
  Route::post('reset', 'PasswordResetController@reset');
});
