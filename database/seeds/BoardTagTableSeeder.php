<?php

use App\Models\Board;
use App\Models\BoardTag;
use App\Models\Tag;
use Illuminate\Database\Seeder;

class BoardTagTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    $boards = Board::all();
    $tags = Tag::all();
    $numberOfTags = count($tags) - 1;
    foreach ($boards as $board) {
      $tagsToAdd = rand(2, $numberOfTags);
      for ($i = 1; $i <= $tagsToAdd; ++$i) {
        $index = rand(0, $numberOfTags);

        $board->boardTags()->save(factory(BoardTag::class)->make([
          'board_id' => $board->id,
          'tag_id'   => $tags[$index]->id
        ]));
      }
    }
  }
}
