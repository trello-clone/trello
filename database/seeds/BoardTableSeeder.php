<?php

use App\Models\Team;
use App\Models\User;
use App\Models\Board;
use Illuminate\Database\Seeder;

class BoardTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    $teams = Team::all();
    foreach ($teams as $team) {
      $team->boards()->saveMany(factory(Board::class, 2)->make([
        'user_id' => $team->user_id
      ]));
    }

    $users = User::all();
    foreach ($users as $user) {
      $user->boards()->saveMany(factory(Board::class, 2)->make());
    }
  }
}
