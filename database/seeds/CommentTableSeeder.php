<?php

use App\Models\Card;
use App\Models\Comment;
use App\Models\User;
use Illuminate\Database\Seeder;

class CommentTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    $cards = Card::all();
    foreach ($cards as $card) {
      $users = $card->collection->board->members;
      foreach ($users as $user) {
        $numberOfComments = rand(0, 4);
        for ($i = 1; $i <= $numberOfComments; ++$i)
          $card->comments()->save(factory(Comment::class)->make([
            'user_id' => $user->id
          ]));
      }
    }
  }
}
