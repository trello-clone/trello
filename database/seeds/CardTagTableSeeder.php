<?php

use App\Models\Card;
use App\Models\CardTag;
use Illuminate\Database\Seeder;

class CardTagTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    $cards = Card::all();
    foreach ($cards as $card) {
      $boardTags = $card->collection->board->boardTags;
      $numberOfBoardTags = count($boardTags) - 1;
      $numberOfTagsToAdd = rand(0, 3);
      for ($i = 1; $i <= $numberOfTagsToAdd; ++$i) {
        $index = rand(0, $numberOfBoardTags);

        $cardTag = new CardTag();
        $cardTag->board_tag_id = $boardTags[$index]->id;
        $cardTag->card_id = $card->id;
        $cardTag->save();
      }
    }
  }
}
