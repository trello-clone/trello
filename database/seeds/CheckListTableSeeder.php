<?php

use App\Models\CheckList;
use App\Models\Card;
use Illuminate\Database\Seeder;

class CheckListTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    $cards = Card::all();
    foreach ($cards as $card) {
      $numberOfCheckLists = rand(0, 3);
      for ($i = 1; $i <= $numberOfCheckLists; ++$i)
        $card->attachments()->save(factory(CheckList::class)->make([
          'order' => $i
        ]));
    }
  }
}
