<?php

use App\Models\CheckList;
use App\Models\CheckListItem;
use Illuminate\Database\Seeder;

class CheckListItemTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    $checkLists = CheckList::all();
    foreach ($checkLists as $checkList) {
      $numberOfItems = rand(2, 10);
      for ($i = 1; $i <= $numberOfItems; ++$i)
        $checkList->items()->save(factory(CheckListItem::class)->make([
          'order' => $i
        ]));
    }
  }
}
