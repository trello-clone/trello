<?php

use Illuminate\Database\Seeder;

class BackgroundImageTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    factory(App\Models\BackgroundImage::class, 10)->create();
  }
}
