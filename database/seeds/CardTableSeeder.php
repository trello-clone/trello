<?php

use App\Models\Card;
use App\Models\Collection;
use Illuminate\Database\Seeder;

class CardTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    $collections = Collection::all();
    foreach ($collections as $collection) {
      $numberOfCards = rand(0, 10);
      for ($i = 1; $i <= $numberOfCards; ++$i)
        $collection->cards()->save(factory(Card::class)->make([
          'order' => $i
        ]));
    }
  }
}
