<?php

use Illuminate\Database\Seeder;
use App\Models\Team;
use App\Models\User;

class TeamTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    $users = User::all();
    foreach ($users as $user) {
      $user->teams()->saveMany(factory(App\Models\Team::class, 2)->make());
    }
  }
}
