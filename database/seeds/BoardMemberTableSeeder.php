<?php

use App\Models\BoardMember;
use App\Models\Board;
use App\Models\User;
use Illuminate\Database\Seeder;

class BoardMemberTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    $boards = Board::all();
    foreach ($boards as $board) {
      $users = factory(User::class, 2)->create();
      foreach ($users as $user) {
        $board_member = new BoardMember();
        $board_member->user_id = $user->id;
        $board_member->board_id = $board->id;
        $board_member->save();
      }
    }
  }
}
