<?php

use App\Models\User;
use App\Models\Team;
use App\Models\TeamInvitation;
use Illuminate\Database\Seeder;

class TeamInvitationTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    $teams = Team::all();
    foreach ($teams as $team) {
      $users = factory(User::class, 3)->create();
      foreach ($users as $user) {
        $invitation = new TeamInvitation();
        $invitation->team_id = $team->id;
        $invitation->sender_user_id = $team->user->id;
        $invitation->receiver_user_id = $user->id;
        $invitation->save();
      }
    }
  }
}
