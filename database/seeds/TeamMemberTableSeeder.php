<?php

use App\Models\Team;
use App\Models\TeamMember;
use App\Models\User;
use Illuminate\Database\Seeder;

class TeamMemberTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    $teams = Team::all();
    foreach ($teams as $team) {
      $users = factory(User::class, 5)->create();
      foreach ($users as $user) {
        $team_member = new TeamMember();
        $team_member->user_id = $user->id;
        $team_member->team_id = $team->id;
        $team_member->save();
      }
    }
  }
}
