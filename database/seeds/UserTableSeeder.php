<?php

use App\Models\User;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    factory(User::class, 2)->create();

    // Set default credentials and data for testing purposes
    $user = User::find(1);
    $user->name = 'Eduardo';
    $user->last_name = 'Fuentes';
    $user->email = 'eduardo.fuentes.rangel@gmail.com';
    $user->password = bcrypt('abc123def456');
    $user->save();
  }
}
