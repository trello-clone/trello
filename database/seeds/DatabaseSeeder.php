<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
  /**
   * Seed the application's database.
   *
   * @return void
   */
  public function run()
  {
    $this->call([
      TagTableSeeder::class,
      BackgroundImageTableSeeder::class,
      BackgroundColorTableSeeder::class,
      UserTableSeeder::class,
      TeamTableSeeder::class,
      BoardTableSeeder::class,
      TeamMemberTableSeeder::class,
      TeamInvitationTableSeeder::class,
      BoardMemberTableSeeder::class,
      BoardInvitationTableSeeder::class,
      BoardTagTableSeeder::class,
      CollectionTableSeeder::class,
      CardTableSeeder::class,
      AttachmentTableSeeder::class,
      CheckListTableSeeder::class,
      CheckListItemTableSeeder::class,
      CommentTableSeeder::class,
      ReactionTableSeeder::class,
      CardTagTableSeeder::class,
      CardUserTableSeeder::class
    ]);
  }
}
