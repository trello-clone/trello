<?php

use App\Models\Board;
use App\Models\BoardInvitation;
use App\Models\User;
use Illuminate\Database\Seeder;

class BoardInvitationTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    $boards = Board::all();
    foreach ($boards as $board) {
      $users = factory(User::class, 3)->create();
      foreach ($users as $user) {
        $invitation = new BoardInvitation();
        $invitation->board_id = $board->id;
        $invitation->sender_user_id = $board->user->id;
        $invitation->receiver_user_id = $user->id;
        $invitation->save();
      }
    }
  }
}
