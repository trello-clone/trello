<?php

use App\Models\Card;
use App\Models\CardUser;
use Illuminate\Database\Seeder;

class CardUserTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    $cards = Card::all();
    foreach ($cards as $card) {
      $members = $card->collection->board->members;
      $numberOfMembers = count($members) - 1;

      $numberOfUsersToAdd = rand(0, 3);
      for ($i = 1; $i <= $numberOfUsersToAdd; ++$i) {
        $index = rand(0, $numberOfMembers);
        $card->members()->save(factory(CardUser::class)->make([
          'user_id' => $members[$index]->id,
          'card_id' => $card->id
        ]));
      }
    }
  }
}
