<?php

use App\Models\Board;
use App\Models\Collection;
use Illuminate\Database\Seeder;

class CollectionTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    $boards = Board::all();
    foreach ($boards as $board) {
      $numberOfCollections = rand(0, 6);
      for ($i = 1; $i <= $numberOfCollections; ++$i)
        $board->collections()->save(factory(Collection::class)->make([
          'order' => $i
        ]));
    }
  }
}
