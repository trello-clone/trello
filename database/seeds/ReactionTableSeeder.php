<?php

use App\Models\User;
use App\Models\Comment;
use App\Models\Reaction;
use Illuminate\Database\Seeder;

class ReactionTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    $comments = Comment::all();
    foreach ($comments as $comment) {
      $users = $comment->card->collection->board->members;
      foreach ($users as $user) {
        $numberOfReactions = rand(0, 2);
        for ($i = 1; $i <= $numberOfReactions; ++$i)
          $comment->reactions()->save(factory(Reaction::class)->make([
            'user_id'    => $user->id,
            'comment_id' => $comment->id
          ]));
      }
    }
  }
}
