<?php

use App\Models\Attachment;
use App\Models\Card;
use Illuminate\Database\Seeder;

class AttachmentTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    $cards = Card::all();
    foreach ($cards as $card) {
      $numberOfAttachments = rand(0, 3);
      for ($i = 1; $i <= $numberOfAttachments; ++$i)
        $card->attachments()->save(factory(Attachment::class)->make([
          'order' => $i
        ]));
    }
  }
}
