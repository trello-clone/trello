<?php

use Illuminate\Database\Seeder;

class BackgroundColorTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    factory(App\Models\BackgroundColor::class, 10)->create();
  }
}
