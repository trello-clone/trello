<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBoardInvitationsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('board_invitations', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->unsignedSmallInteger('status')->default(0);
      $table->string('message')->default('');
      $table->unsignedBigInteger('board_id');
      $table->foreign('board_id')->references('id')->on('boards');
      $table->unsignedBigInteger('sender_user_id');
      $table->foreign('sender_user_id')->references('id')->on('users');
      $table->unsignedBigInteger('receiver_user_id');
      $table->foreign('receiver_user_id')->references('id')->on('users');
      $table->timestamps();
      $table->softDeletes();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('board_invitations');
  }
}
