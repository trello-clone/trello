<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCheckListsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('check_lists', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->string('title');
      $table->longText('description');
      $table->boolean('hide_completed_items')->default(false);
      $table->unsignedInteger('order');
      $table->unsignedBigInteger('card_id');
      $table->foreign('card_id')->references('id')->on('cards');
      $table->timestamps();
      $table->softDeletes();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('check_lists');
  }
}
