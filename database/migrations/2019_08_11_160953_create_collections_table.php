<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCollectionsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('collections', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->string('title');
      $table->boolean('is_archived')->default(false);
      $table->unsignedInteger('order');
      $table->unsignedBigInteger('board_id');
      $table->foreign('board_id')->references('id')->on('boards');
      $table->timestamps();
      $table->softDeletes();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('collections');
  }
}
