<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBoardTagsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('board_tags', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->string('name')->default('');
      $table->unsignedBigInteger('tag_id');
      $table->foreign('tag_id')->references('id')->on('tags');
      $table->unsignedBigInteger('board_id');
      $table->foreign('board_id')->references('id')->on('boards');
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('board_tags');
  }
}
