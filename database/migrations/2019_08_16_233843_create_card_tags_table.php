<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCardTagsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('card_tags', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->unsignedBigInteger('board_tag_id');
      $table->foreign('board_tag_id')->references('id')->on('board_tags');
      $table->unsignedBigInteger('card_id');
      $table->foreign('card_id')->references('id')->on('cards');
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('card_tags');
  }
}
