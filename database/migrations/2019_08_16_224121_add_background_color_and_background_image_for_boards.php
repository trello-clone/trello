<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBackgroundColorAndBackgroundImageForBoards extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::table('boards', function (Blueprint $table) {
      $table->unsignedBigInteger('background_image_id')->nullable();
      $table->foreign('background_image_id')->references('id')->on('background_images');
      $table->unsignedBigInteger('background_color_id')->nullable();
      $table->foreign('background_color_id')->references('id')->on('background_colors');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::table('boards', function (Blueprint $table) {
      $table->dropForeign(['background_image_id']);
      $table->dropForeign(['background_color_id']);
      $table->dropColumn(['background_image_id', 'background_color_id']);
    });
  }
}
