<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttachmentsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('attachments', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->string('name')->default('');
      $table->unsignedInteger('order');
      $table->string('file');
      $table->string('mime_type');
      $table->unsignedBigInteger('card_id');
      $table->foreign('card_id')->references('id')->on('cards');
      $table->timestamps();
      $table->softDeletes();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('attachments');
  }
}
