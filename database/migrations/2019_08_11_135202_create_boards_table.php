<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBoardsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('boards', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->string('title');
      $table->longText('description');
      $table->smallInteger('visibility')->default(0);
      $table->boolean('daltonic_mode')->default(false);
      $table->unsignedBigInteger('user_id');
      $table->foreign('user_id')->references('id')->on('users');
      $table->unsignedBigInteger('team_id')->nullable();
      $table->foreign('team_id')->references('id')->on('teams');
      $table->timestamps();
      $table->softDeletes();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('boards');
  }
}
