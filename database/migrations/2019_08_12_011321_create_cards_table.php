<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCardsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('cards', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->string('title');
      $table->string('description', 300);
      $table->unsignedInteger('order');
      $table->boolean('is_archived')->default(false);
      $table->boolean('is_completed')->default(false);
      $table->timestamp('expires_at')->nullable();
      $table->unsignedBigInteger('collection_id');
      $table->foreign('collection_id')->references('id')->on('collections');
      $table->timestamps();
      $table->softDeletes();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('cards');
  }
}
