<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTeamInvitationsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('team_invitations', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->unsignedSmallInteger('status')->default(0);
      $table->string('message')->default('');
      $table->unsignedBigInteger('team_id');
      $table->foreign('team_id')->references('id')->on('teams');
      $table->unsignedBigInteger('sender_user_id');
      $table->foreign('sender_user_id')->references('id')->on('users');
      $table->unsignedBigInteger('receiver_user_id');
      $table->foreign('receiver_user_id')->references('id')->on('users');
      $table->timestamps();
      $table->softDeletes();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('team_invitations');
  }
}
