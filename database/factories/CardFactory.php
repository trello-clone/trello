<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Card;
use Faker\Generator as Faker;

$factory->define(Card::class, function (Faker $faker) {
  return [
    'title'       => $faker->text(25),
    'description' => $faker->text(180)
  ];
});
