<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\BackgroundImage;
use Faker\Generator as Faker;

$factory->define(BackgroundImage::class, function (Faker $faker) {
  return [
    'background_image' => $faker->imageUrl()
  ];
});
