<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Board;
use Faker\Generator as Faker;

$factory->define(Board::class, function (Faker $faker) {

  $background_image = null;
  $background_color = null;
  $number = rand(1, 10);
  if ($number >= 5)
    $background_image = $number;
  else
    $background_color = $number;

  return [
    'title'       => $faker->text(25),
    'description' => $faker->text(120),
    'visibility'  => true,
    'background_image_id' => $background_image,
    'background_color_id' => $background_color
  ];
});
