<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\BoardTag;
use Faker\Generator as Faker;

$factory->define(BoardTag::class, function (Faker $faker) {
  return [
    'name' => $faker->sentence(1)
  ];
});
