<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\CardUser;
use Faker\Generator as Faker;

$factory->define(CardUser::class, function (Faker $faker) {
  return [
    'is_follower' => (rand(1, 10) >= 5)
  ];
});
