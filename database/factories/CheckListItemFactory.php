<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\CheckListItem;
use Faker\Generator as Faker;

$factory->define(CheckListItem::class, function (Faker $faker) {
  return [
    'text'         => $faker->text(120),
    'is_completed' => (rand(1, 10) >= 5)
  ];
});
