<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\BackgroundColor;
use Faker\Generator as Faker;

$factory->define(BackgroundColor::class, function (Faker $faker) {
  return [
    'color' => $faker->hexColor
  ];
});
