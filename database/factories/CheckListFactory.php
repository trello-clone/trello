<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\CheckList;
use Faker\Generator as Faker;

$factory->define(CheckList::class, function (Faker $faker) {
  return [
    'title'       => $faker->text(25),
    'description' => $faker->sentence(20)
  ];
});
