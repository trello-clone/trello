<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Attachment;
use Faker\Generator as Faker;

$factory->define(Attachment::class, function (Faker $faker) {
  return [
    'name'      => $faker->name,
    'file'      => $faker->name . '.' . $faker->fileExtension,
    'mime_type' => $faker->mimeType
  ];
});
