<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Team;
use Faker\Generator as Faker;

$factory->define(Team::class, function (Faker $faker) {
  return [
    'name'             => $faker->text(25),
    'background_image' => $faker->imageUrl(),
    'website'          => $faker->url,
    'description'      => $faker->text(120)
  ];
});
