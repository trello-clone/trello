<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Reaction;
use Faker\Generator as Faker;

$factory->define(Reaction::class, function (Faker $faker) {
  return [
    'emoji' => $faker->sentence(1)
  ];
});
