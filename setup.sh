#!/bin/bash

echo -e "Executing migrations..."
php artisan migrate:refresh

echo -e "\nCreating passport credentials..."
php artisan passport:install --force

echo -e "\nSeeding database..."
php artisan db:seed