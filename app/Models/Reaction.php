<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Reaction extends Model
{
  /**
   * The user to which the reaction belongs to
   *
   * @return Illuminate\Database\Eloquent\Relations\Relation
   */
  public function user()
  {
    return $this->belongsTo(User::class, 'user_id');
  }

  /**
   * The comment to which the reaction belongs to
   *
   * @return Illuminate\Database\Eloquent\Relations\Relation
   */
  public function comment()
  {
    return $this->belongsTo(Comment::class, 'comment_id');
  }
}
