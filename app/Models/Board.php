<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Board extends Model
{
  use SoftDeletes;

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'title', 'description',
    'visibility', 'daltonic_mode',
    'user_id', 'team_id'
  ];

  /**
   * Board's owner
   *
   * @return Illuminate\Database\Eloquent\Relations\Relation
   */
  public function user()
  {
    return $this->belongsTo(User::class, 'user_id');
  }

  /**
   * A board can have many members
   *
   * @return Illuminate\Database\Eloquent\Relations\Relation
   */
  public function members()
  {
    return $this->hasMany(BoardMember::class, 'board_id');
  }

  /**
   * Invitations to join the board
   *
   * @return Illuminate\Database\Eloquent\Relations\Relation
   */
  public function invitations()
  {
    return $this->hasMany(BoardInvitation::class, 'board_id');
  }

  /**
   * A board might belong to a team
   *
   * @return Illuminate\Database\Eloquent\Relations\Relation
   */
  public function team()
  {
    return $this->belongsTo(Team::class, 'team_id');
  }

  /**
   * A board might had a background image
   *
   * @return Illuminate\Database\Eloquent\Relations\Relation
   */
  public function backgroundImage()
  {
    return $this->belongsTo(BackgroundImage::class, 'background_image_id');
  }

  /**
   * A board might had a background color
   *
   * @return Illuminate\Database\Eloquent\Relations\Relation
   */
  public function backgroundColor()
  {
    return $this->belongsTo(BackgroundColor::class, 'background_color_id');
  }

  /**
   * A board can have many collections
   *
   * @return Illuminate\Database\Eloquent\Relations\Relation
   */
  public function collections()
  {
    return $this->hasMany(Collection::class, 'board_id');
  }

  /**
   * A board can have many tags
   *
   * @return Illuminate\Database\Eloquent\Relations\Relation
   */
  public function boardTags()
  {
    return $this->hasMany(BoardTag::class, 'board_id');
  }
}
