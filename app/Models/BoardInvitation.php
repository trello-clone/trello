<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BoardInvitation extends Model
{
  /**
   * The board to join
   *
   * @return Illuminate\Database\Eloquent\Relations\Relation
   */
  public function board()
  {
    return $this->belongsTo(Board::class, 'board_id');
  }

  /**
   * The user who sent the invitation
   *
   * @return Illuminate\Database\Eloquent\Relations\Relation
   */
  public function sender()
  {
    return $this->belongsTo(User::class, 'sender_user_id');
  }

  /**
   * The user who received the invitation
   *
   * @return Illuminate\Database\Eloquent\Relations\Relation
   */
  public function receiver()
  {
    return $this->belongsTo(User::class, 'receiver_user_id');
  }
}
