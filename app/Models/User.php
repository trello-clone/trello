<?php

namespace App\Models;

use Storage;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
  use Notifiable, HasApiTokens, SoftDeletes;

  /**
   * The attributtes that will be append when we get a user
   *
   * @var array
   */
  protected $appends = ['avatar_url'];

  protected $dates = ['deleted_at'];

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'name', 'last_name',
    'email', 'password',
    'active', 'activation_token',
    'avatar'
  ];

  /**
   * The attributes that should be hidden for arrays.
   *
   * @var array
   */
  protected $hidden = [
    'password', 'remember_token', 'activation_token'
  ];

  /**
   * The attributes that should be cast to native types.
   *
   * @var array
   */
  protected $casts = [
    'email_verified_at' => 'datetime',
  ];

  public function getAvatarUrlAttribute()
  {
    return Storage::url('avatars/' . $this->avatar);
  }

  /**
   * A user can have many boards
   *
   * @return Illuminate\Database\Eloquent\Relations\Relation
   */
  public function boards()
  {
    return $this->hasMany(Board::class, 'user_id');
  }

  /**
   * A user can have many teams
   *
   * @return Illuminate\Database\Eloquent\Relations\Relation
   */
  public function teams()
  {
    return $this->hasMany(Team::class, 'user_id');
  }

  /**
   * Invitations the user has sent for a board
   *
   * @return Illuminate\Database\Eloquent\Relations\Relation
   */
  public function hasInvitedToBoard()
  {
    return $this->hasMany(BoardInvitation::class, 'sender_user_id');
  }

  /**
   * Invitations the user has received to join a board
   *
   * @return Illuminate\Database\Eloquent\Relations\Relation
   */
  public function hasBeingInvitedToBoard()
  {
    return $this->hasMany(BoardInvitation::class, 'receiver_user_id');
  }

  /**
   * Invitations the user has sent for a team
   *
   * @return Illuminate\Database\Eloquent\Relations\Relation
   */
  public function hasInvitedToTeam()
  {
    return $this->hasMany(TeamInvitation::class, 'sender_user_id');
  }

  /**
   * Invitations the user has received to join a team
   *
   * @return Illuminate\Database\Eloquent\Relations\Relation
   */
  public function hasBeingInvitedToTeam()
  {
    return $this->hasMany(TeamInvitation::class, 'receiver_user_id');
  }

  /**
   * Boards the user is a member of
   *
   * @return Illuminate\Database\Eloquent\Relations\Relation
   */
  public function memberOfBoards()
  {
    return $this->hasMany(BoardMember::class, 'user_id');
  }

  /**
   * Teams the user is a member of
   *
   * @return Illuminate\Database\Eloquent\Relations\Relation
   */
  public function memberOfTeams()
  {
    return $this->hasMany(TeamMember::class, 'user_id');
  }
}
