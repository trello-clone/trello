<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CardUser extends Model
{
  use SoftDeletes;

  /**
   * The user to which the card user belongs to
   *
   * @return Illuminate\Database\Eloquent\Relations\Relation
   */
  public function user()
  {
    return $this->belongsTo(User::class, 'user_id');
  }

  /**
   * The card to which the card user belongs to
   *
   * @return Illuminate\Database\Eloquent\Relations\Relation
   */
  public function card()
  {
    return $this->belongsTo(Card::class, 'card_id');
  }
}
