<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Team extends Model
{
  use SoftDeletes;

  /**
   * A team can have many members
   *
   * @return Illuminate\Database\Eloquent\Relations\Relation
   */
  public function members()
  {
    return $this->hasMany(TeamMember::class, 'user_id');
  }

  /**
   * The team's owner
   *
   * @return Illuminate\Database\Eloquent\Relations\Relation
   */
  public function user()
  {
    return $this->belongsTo(User::class, 'user_id');
  }

  /**
   * Invitations to join the team
   *
   * @return Illuminate\Database\Eloquent\Relations\Relation
   */
  public function invitations()
  {
    return $this->hasMany(TeamInvitation::class, 'team_id');
  }

  /**
   * A team can have many boards
   *
   * @return Illuminate\Database\Eloquent\Relations\Relation
   */
  public function boards()
  {
    return $this->hasMany(Board::class, 'team_id');
  }
}
