<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BoardTag extends Model
{
  /**
   * The tag to which the board tag belongs to
   *
   * @return Illuminate\Database\Eloquent\Relations\Relation
   */
  public function tag()
  {
    return $this->belongsTo(Tag::class, 'tag_id');
  }

  /**
   * The board to which the board tag belongs to
   *
   * @return Illuminate\Database\Eloquent\Relations\Relation
   */
  public function board()
  {
    return $this->belongsTo(Board::class, 'board_id');
  }
}
