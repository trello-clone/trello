<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TeamMember extends Model
{
  /**
   * The user that belongs to the team
   *
   * @return Illuminate\Database\Eloquent\Relations\Relation
   */
  public function user()
  {
    return $this->belongsTo(User::class, 'user_id');
  }

  /**
   * The team to which the user is a member of
   *
   * @return Illuminate\Database\Eloquent\Relations\Relation
   */
  public function team()
  {
    return $this->belongsTo(Team::class, 'team_id');
  }
}
