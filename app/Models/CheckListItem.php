<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CheckListItem extends Model
{
  /**
   * The check list to which the item belongs to
   *
   * @return Illuminate\Database\Eloquent\Relations\Relation
   */
  public function checkList()
  {
    return $this->belongsTo(CheckList::class, 'check_list_id');
  }
}
