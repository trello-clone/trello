<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
  /**
   * The card to which the comment belongs to
   *
   * @return Illuminate\Database\Eloquent\Relations\Relation
   */
  public function card()
  {
    return $this->belongsTo(Card::class, 'card_id');
  }

  /**
   * The user to which the comment belongs to
   *
   * @return Illuminate\Database\Eloquent\Relations\Relation
   */
  public function user()
  {
    return $this->belongsTo(User::class, 'user_id');
  }

  /**
   * A comment can have many reactions
   *
   * @return Illuminate\Database\Eloquent\Relations\Relation
   */
  public function reactions()
  {
    return $this->hasMany(Reaction::class, 'comment_id');
  }
}
