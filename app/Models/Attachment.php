<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Attachment extends Model
{
  /**
   * The card to which the file belongs to
   *
   * @return Illuminate\Database\Eloquent\Relations\Relation
   */
  public function card()
  {
    return $this->belongsTo(Card::class, 'card_id');
  }
}
