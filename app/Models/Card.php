<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Card extends Model
{
  use SoftDeletes;

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'title', 'description', 'order',
    'is_archived', 'is_completed',
    'expires_at'
  ];

  /**
   * The collection to which the card belongs to
   *
   * @return Illuminate\Database\Eloquent\Relations\Relation
   */
  public function collection()
  {
    return $this->belongsTo(Collection::class, 'collection_id');
  }

  /**
   * A card can have many attachments
   *
   * @return Illuminate\Database\Eloquent\Relations\Relation
   */
  public function attachments()
  {
    return $this->hasMany(Attachment::class, 'card_id');
  }

  /**
   * A card can have many tags
   *
   * @return Illuminate\Database\Eloquent\Relations\Relation
   */
  public function cardTags()
  {
    return $this->hasMany(CardTag::class, 'card_id');
  }

  /**
   * A card can have many comments
   *
   * @return Illuminate\Database\Eloquent\Relations\Relation
   */
  public function comments()
  {
    return $this->hasMany(Comment::class, 'card_id');
  }

  /**
   * A card can have many followers
   *
   * @return Illuminate\Database\Eloquent\Relations\Relation
   */
  public function followers()
  {
    return $this->hasMany(CardUser::class, 'card_id')->where('is_follower', true);
  }

  /**
   * A card can have many members
   *
   * @return Illuminate\Database\Eloquent\Relations\Relation
   */
  public function members()
  {
    return $this->hasMany(CardUser::class, 'card_id')->where('is_follower', false);
  }

  /**
   * A card can have many check lists
   *
   * @return Illuminate\Database\Eloquent\Relations\Relation
   */
  public function checkLists()
  {
    return $this->hasMany(CheckList::class, 'card_id');
  }
}
