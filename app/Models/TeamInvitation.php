<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TeamInvitation extends Model
{
  /**
   * The team to join
   *
   * @return Illuminate\Database\Eloquent\Relations\Relation
   */
  public function team()
  {
    return $this->belongsTo(Team::class, 'team_id');
  }

  /**
   * The user who sent the invitation
   *
   * @return Illuminate\Database\Eloquent\Relations\Relation
   */
  public function sender()
  {
    return $this->belongsTo(User::class, 'sender_user_id');
  }

  /**
   * The user who received the invitation
   *
   * @return Illuminate\Database\Eloquent\Relations\Relation
   */
  public function receiver()
  {
    return $this->belongsTo(User::class, 'receiver_user_id');
  }
}
