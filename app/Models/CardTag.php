<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CardTag extends Model
{
  /**
   * The board tag to which the card tag belongs to
   *
   * @return Illuminate\Database\Eloquent\Relations\Relation
   */
  public function boardTag()
  {
    return $this->belongsTo(BoardTag::class, 'board_tag_id');
  }

  /**
   * The card to which the card tag belongs to
   *
   * @return Illuminate\Database\Eloquent\Relations\Relation
   */
  public function card()
  {
    return $this->belongsTo(Card::class, 'card_id');
  }
}
