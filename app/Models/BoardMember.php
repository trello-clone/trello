<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BoardMember extends Model
{
  /**
   * The user that belongs to the board
   *
   * @return Illuminate\Database\Eloquent\Relations\Relation
   */
  public function user()
  {
    return $this->belongsTo(User::class, 'user_id');
  }

  /**
   * The board to which the user is a member of
   *
   * @return Illuminate\Database\Eloquent\Relations\Relation
   */
  public function board()
  {
    return $this->belongsTo(Board::class, 'board_id');
  }
}
