<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CheckList extends Model
{
  use SoftDeletes;

  /**
   * The card to which the check list belongs to
   *
   * @return Illuminate\Database\Eloquent\Relations\Relation
   */
  public function card()
  {
    return $this->belongsTo(Card::class, 'card_id');
  }

  /**
   * A check list can have many items
   *
   * @return Illuminate\Database\Eloquent\Relations\Relation
   */
  public function items()
  {
    return $this->hasMany(CheckListItem::class, 'check_list_id');
  }
}
