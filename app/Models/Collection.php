<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Collection extends Model
{
  use SoftDeletes;

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'title', 'is_archived', 'order'
  ];

  /**
   * Board to which the collection belongs to
   *
   * @return Illuminate\Database\Eloquent\Relations\Relation
   */
  public function board()
  {
    return $this->belongsTo(Board::class, 'board_id');
  }

  /**
   * A collection can have many cards
   *
   * @return Illuminate\Database\Eloquent\Relations\Relation
   */
  public function cards()
  {
    return $this->hasMany(Card::class, 'collection_id');
  }
}
