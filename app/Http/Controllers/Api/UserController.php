<?php

namespace App\Http\Controllers\Api;

use Avatar;
use Storage;
use Webpatser\Uuid\Uuid;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

class UserController extends Controller
{
  public function updateProfile(Request $request)
  {
    $this->validate($request, [
      'name'      => 'required|string',
      'last_name' => 'required|string'
    ]);

    $name = '';

    $user = $request->user();

    if ($request->hasFile('avatar')) {
      $extension = $request->file('avatar')->getClientOriginalExtension();
      Storage::delete('public/avatars/' . $user->avatar);
      $name = Uuid::generate()->string  . '.' . $extension;
      $request->file('avatar')->storeAs('public/avatars', $name);
    }

    $user->name = $request->input('name');
    $user->last_name = $request->input('last_name');
    if ($request->hasFile('avatar')) {
      $user->avatar = $name;
    }

    $user->save();

    return response()->json($user);
  }

  public function updatePassword(Request $request)
  {
    $request->validate([
      'current_password' => 'required|string',
      'password'         => 'required|string|confirmed|min:10|different:current_password'
    ]);

    $user = $request->user();

    if (!Hash::check($request->current_password, $user->password)) {
      return response()->json([
        'message' => 'The current password is not correct'
      ], 404);
    }

    $user->password = Hash::make($request->password);
    $user->save();

    return response()->json([
      'message' => 'The password has been successfully updated'
    ]);
  }
}
