<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

use Avatar;
use Storage;
use Webpatser\Uuid\Uuid;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Notifications\AccountActivation;

class AuthController extends Controller
{
  public function register(Request $request)
  {
    $request->validate([
      'name'      => 'required|string',
      'last_name' => 'required|string',
      'email'     => 'required|string|email|unique:users',
      'password'  => 'required|string|confirmed|min:10'
    ]);

    $avatar = Avatar::create($request->name . ' ' . $request->last_name)
      ->setDimension(200)
      ->getImageObject()
      ->encode('png');
    $name = Uuid::generate()->string . '.png';
    Storage::put('public/avatars/' . $name, (string) $avatar);

    $user = new User([
      'name'             => $request->name,
      'last_name'        => $request->last_name,
      'email'            => $request->email,
      'password'         => Hash::make($request->password),
      'activation_token' => str_random(60),
      'avatar'           => $name
    ]);

    $user->save();

    $user->notify(new AccountActivation($user));

    return response()->json([
      'message' => 'Successfully created user!'
    ], 201);
  }

  public function login(Request $request)
  {
    $request->validate([
      'email'       => 'required|string|email',
      'password'    => 'required|string',
      'remember_me' => 'boolean'
    ]);

    $credentials = request(['email', 'password']);
    $credentials['active'] = 1;
    $credentials['deleted_at'] = null;

    if (!Auth::attempt($credentials)) {
      return response()->json([
        'message' => 'Unauthorized'
      ], 401);
    }

    $user = $request->user();
    $tokenResult = $user->createToken('Personal Access Token');
    $token = $tokenResult->token;

    if ($request->remember_me) {
      $token->expires_at = Carbon::now()->addWeeks(1);
    }

    $token->save();

    return response()->json([
      'access_token' => $tokenResult->accessToken,
      'token_type'   => 'Bearer',
      'expires_at'   => Carbon::parse(
        $tokenResult->token->expires_at
      )->toDateTimeString()
    ]);
  }

  public function logout(Request $request)
  {
    $request->user()->token()->revoke();

    return response()->json(['message' => 'Successfully logout']);
  }

  public function me(Request $request)
  {
    return response()->json($request->user());
  }

  public function registerActivate($token)
  {
    $user = User::where('activation_token', $token)->first();

    if (!$user) {
      return response()->json(['message' => 'The activation token is invalid'], 404);
    }

    $user->active = true;
    $user->activation_token = '';
    $user->save();

    return $user;
  }
}
